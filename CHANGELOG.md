0.2.2
* PATCH Shared memory update after cooldown
* Fix a typos in log output and function name
* Confirmed Operation on the Argon One m.2 case
* Confirmed Ubuntu is a fully working

0.2.1
* PATCH for Argon Artik Fan hat
* Added OS support for Gentoo and Manjaro
* Change Build Procedure for new OS support

0.2.0
* Added shared memory interface
* Changed Logging system
* Added CLI program to interface with daemon
 

0.1.6  
* fix signed unsigned conversion  
* fix unexpected exit if pulse width out of bounds  
0.1.5 Initial Release